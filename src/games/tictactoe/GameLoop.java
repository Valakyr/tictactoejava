package games.tictactoe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

public class GameLoop {
	private PrintStream out;
	private BufferedReader in;
	
	public GameLoop(PrintStream out, BufferedReader in) {
		this.out = out;
		this.in = in;
	}
	
	public void run() throws IOException {
		Player[] players = new Player[2];
		init(players);
		int startPlayerIndex = randomStartPlayer();
		
		while (true) {
			playGame(players, startPlayerIndex);
			startPlayerIndex = (startPlayerIndex + 1) % 2;
			
			out.println("press enter to start the next game");
			in.readLine();
		}
	}
	
	public void init(Player[] players) throws IOException {
		players[0] = new Player();
		players[1] = new Player();
		char firstPiece = initPlayer(0, players[0], Optional.empty());
		initPlayer(1, players[1], Optional.of(firstPiece));
	}
	
	public int randomStartPlayer() {
		int startPlayerIndex = ThreadLocalRandom.current().nextInt(0, 2);
		out.println(String.format("Player %d begins", startPlayerIndex));
		return startPlayerIndex;
	}
	
	public char initPlayer(int playerNumber, Player player, Optional<Character> pieceTaken) throws IOException {
		if (playerNumber < 0 || playerNumber > 2)
			throw new IllegalArgumentException(String.format("player index out of range: %d", playerNumber));
		
		out.println(String.format("Player %d, enter name: ", playerNumber + 1));
		String name = in.readLine().trim();
		player.setName(name);
		
		char piece;
		if (!pieceTaken.isPresent()) {
			out.println(String.format("Player %d, enter piece symbol ('X' or 'O'): ", playerNumber + 1));
			String pieceStr;
			while (true) {
				pieceStr = in.readLine().trim();
				if (!pieceStr.equals("X") && !pieceStr.equals("O")) {
					out.println("illegal piece symbol, enter piece symbol ('X' or 'O'): ");
				}
				else {
					break;
				}
			}
			piece = pieceStr.charAt(0);
		}
		else {
			if (pieceTaken.get() == 'X') {
				piece = 'O';
			} else {
				piece = 'X';
			}
		}
		player.setPiece(piece);
		return piece;
	}

	public void playGame(Player[] players, int startPlayerIndex) throws IOException {
		Game game = new Game(players, startPlayerIndex);
		Board board = new Board();
		while (true) {
			board.print(out);
			int move = -1;
			while (true) {
				out.println(String.format("Player %s, please enter move (1-9)", game.getActivePlayer().getName()));
				String moveStr = in.readLine();
				try {
					move = Integer.parseInt(moveStr);
				} catch (NumberFormatException ex) {
					out.println("enter number between 1 and 9");
					continue;
				}
				move--;
				if (move < 0 || move > 8 || board.isOccupied(move))
					out.println("not a legal move");
				else
					break;
			}
			
			char activePlayerPiece = game.getActivePlayer().getPiece();
			board.set(activePlayerPiece, move);
			
			if (game.hasActivePlayerWon(board)) {
				board.print(out);
				out.println(String.format("Player %s has won!", game.getActivePlayer().getName()));
				break;
			}
			game.nextPlayer();
			if (game.IsDraw()) {
				board.print(out);
				out.println("Game is a draw.");
				break;
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		GameLoop gameLoop = new GameLoop(System.out, new BufferedReader(new InputStreamReader(System.in)));
		gameLoop.run();
	}
}
