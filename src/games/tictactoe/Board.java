package games.tictactoe;

import java.io.PrintStream;

public class Board {
	private static int[] boardPositions = {
		16, 20, 24, 44, 48, 52, 72, 76, 80 	
	};
	private char[] graphics = new char[] {
		'|', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '|', '\n',	
		'|', ' ', '1', ' ', '|', ' ', '2', ' ', '|', ' ', '3', ' ', '|', '\n',
		'|', '-', '-', '-', '|', '-', '-', '-', '|', '-', '-', '-', '|', '\n',
		'|', ' ', '4', ' ', '|', ' ', '5', ' ', '|', ' ', '6',' ',  '|', '\n',
		'|', '-', '-', '-', '|', '-', '-', '-', '|', '-', '-', '-', '|', '\n',
		'|', ' ', '7', ' ', '|', ' ', '8', ' ', '|', ' ', '9',' ',  '|', '\n',
		'|', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '|', '\n'
	};
	
	public void print(PrintStream stream) {
		for (int i=0; i < graphics.length; i++) {
			stream.print(graphics[i]);
		}
	}
	
	public void reset() {
		for (int i = 0; i < boardPositions.length; i++) {
			graphics[boardPositions[i]] = Character.forDigit(i + 1, 10);
		}
	}
	
	public void set(char piece, int position) {
		checkPosition(position);
		if (piece != 'X' && piece != 'O')
			throw new IllegalArgumentException(String.format("invalid piece: %s", piece));
		
		graphics[boardPositions[position]] = piece;
	}
	
	public char get(int position) {
		checkPosition(position);
		return graphics[boardPositions[position]];
	}
	
	public boolean isOccupied(int position) {
		char piece = get(position);
		return piece == 'O' || piece == 'X';
	}
	
	private void checkPosition(int position) {
		if (position < 0 || position > 8)
			throw new IllegalArgumentException(String.format("invalid position: %d", position));
	}
}
