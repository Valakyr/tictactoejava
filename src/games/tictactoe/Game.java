package games.tictactoe;

public class Game {
	private Player[] players = new Player[2];
	private int activePlayerIndex;
	private int currentTurn;
	
	public Game(Player[] players, int activePlayerIndex) {
		if (activePlayerIndex < 0 || activePlayerIndex > 1)
			throw new IllegalArgumentException(String.format("player index out of range: %d", activePlayerIndex));
		if (players == null || players.length != 2 || players[0] == null || players[1] == null)
			throw new IllegalArgumentException("expected exactly two players");
		this.players = players;
		this.activePlayerIndex = activePlayerIndex;
		currentTurn = 0;
	}
	
	public Player getActivePlayer() {
		checkInitialized();
		return players[activePlayerIndex];
	}
	
	public void nextPlayer() {
		checkInitialized();
		activePlayerIndex = (activePlayerIndex + 1) % 2;
		currentTurn++;
	}
	
	public int getCurrentTurn() {
		return currentTurn;
	}
	
	public boolean hasActivePlayerWon(Board board) {
		if (board == null)
			throw new IllegalArgumentException("board is null");
		checkInitialized();
		
		char piece = getActivePlayer().getPiece();
		
		// rows
		for (int i = 0; i < 3; i++) {
			int offset = i * 3;
			boolean filled = board.get(offset) == piece
					&& board.get(offset + 1) == piece
					&& board.get(offset + 2) == piece;
			if (filled)
				return true;
		}
		
		// columns
		for (int i = 0; i < 3; i++) {
			boolean filled = board.get(i) == piece
					&& board.get(i + 3) == piece
					&& board.get(i + 6) == piece;
			if (filled)
				return true;
		}
		
		// diagonals
		return (board.get(0) == piece && board.get(4) == piece && board.get(8) == piece)
				|| (board.get(2) == piece && board.get(4) == piece && board.get(6) == piece);
	}

	public boolean IsDraw() {
		return currentTurn > 8;
	}
	
	private void checkInitialized() {
		if (players == null)
			throw new IllegalStateException("players not initialized");
	}
}
